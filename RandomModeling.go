package main

import (
	"fmt"
	"magister_fast/database"
	"math/rand"
	"time"
)

type RandomModeling struct {
	db *database.Database
}

func (rm *RandomModeling) Make() error {
	return nil
}

func (rm *RandomModeling) Before(ename string, remod bool) error {
	rand.Seed(time.Now().Unix())
	rm.db = database.NewDatabase(baseDir)
	if remod {
	} else {
		rm.db.World = database.World{
			LastID:      1,
			Step:        0,
			RandomStart: 0.75,
			RandomLen:   0.5,
		}
		rm.db.World.HeroHPRun = rm.db.World.GetRand() * 0.3
		rm.db.World.HeroVamp = vampire
		rm.db.World.MonsterHPRun = rm.db.World.GetRand() * 0.3
		rm.db.World.MonsterVamp = vampire
		// добавление генератора монстров
		rm.db.AddMG(&database.EntityGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: int64(50 * rm.db.World.GetRand()),
				Start:  1,
				Count:  int(20 * rm.db.World.GetRand()),
				XDev:   1000 * rm.db.World.GetRand(),
				XMean:  0,
				YDev:   1000 * rm.db.World.GetRand(),
				YMean:  0,
			},
			DamageMean:       50 * rm.db.World.GetRand(),
			DamageDev:        5,
			MaxHPMean:        100 * rm.db.World.GetRand(),
			MaxHPDev:         10,
			HPPSMean:         10 * rm.db.World.GetRand(),
			HPPSDev:          3,
			AttackRadiusMean: 10 * rm.db.World.GetRand(),
			AttackRadiusDev:  3,
			MaxMoveMean:      5 * rm.db.World.GetRand(),
			MaxMoveDev:       1,
		})
		// добавление генератора героев
		rm.db.AddHG(&database.EntityGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: int64(50 * rm.db.World.GetRand()),
				Start:  1,
				Count:  int(20 * rm.db.World.GetRand()),
				XDev:   1000 * rm.db.World.GetRand(),
				XMean:  0,
				YDev:   1000 * rm.db.World.GetRand(),
				YMean:  0,
			},
			DamageMean:       50 * rm.db.World.GetRand(),
			DamageDev:        5,
			MaxHPMean:        100 * rm.db.World.GetRand(),
			MaxHPDev:         10,
			HPPSMean:         10 * rm.db.World.GetRand(),
			HPPSDev:          3,
			AttackRadiusMean: 10 * rm.db.World.GetRand(),
			AttackRadiusDev:  3,
			MaxMoveMean:      5 * rm.db.World.GetRand(),
			MaxMoveDev:       1,
		})
		// добавление генератора крепостей
		rm.db.AddFG(&database.FortressGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: int64(100 * rm.db.World.GetRand()),
				Start:  1,
				Count:  int(5 * rm.db.World.GetRand()),
				XDev:   1000 * rm.db.World.GetRand(),
				XMean:  0,
				YDev:   1000 * rm.db.World.GetRand(),
				YMean:  0,
			},
			DamageMean:       50 * rm.db.World.GetRand(),
			DamageDev:        5,
			AttackRadiusMean: 20 * rm.db.World.GetRand(),
			AttackRadiusDev:  5,
			LiveTimeStart:    int64(500 * rm.db.World.GetRand()),
			LiveTimeLen:      int64(500 * rm.db.World.GetRand()),
		})
	}
	return nil
}
func (rm *RandomModeling) Step(i int) error { return nil }
func (rm *RandomModeling) Info() string {
	return fmt.Sprintf(
		"step %d monster count %d hero count %d fortress count %d",
		rm.db.World.Step,
		len(rm.db.Monsters),
		len(rm.db.Heroes),
		len(rm.db.Fortresses),
	)
}
func (rm *RandomModeling) DB() *database.Database { return rm.db }
