import easygraph
import json
import tkinter
import copy
import sys

CORES = [
    lambda x: 1-abs(x) if abs(x) <= 1 else 0,
    lambda x: 0.75*(1-x**2) if abs(x) <= 1 else 0,
]
METRICS = [
    lambda x, y: sum([(j-i)**2 for i, j in zip(x, y)])**0.5,
    lambda x, y: sum([abs(j-i) for i, j in zip(x, y)]),
]


def load_data(exp_name):
    tmp = json.load(open('data/{0}/stat.json'.format(exp_name)))
    y0 = [e['y'] for e in tmp['мера количества']]
    y1 = [0, ]+[e['y'] for e in tmp['dFp/dt']]
    y2 = [0, 0]+[e['y'] for e in tmp['2dFp/dt^2']]
    return y0, y1, y2


def mul(m):
    s = 1
    for i in m:
        s *= i
    return s


def x_estimation(x, u, vu, cs, core, cross_ex=None):
    n = len(x)
    # ~ n=10
    n1 = len(vu)
    try:
        xs = sum([mul([core((vu[j]-u[i][j])/cs) for j in range(n1)])*x[i]
                  for i in range(n) if (cross_ex != None and (i != cross_ex)) or (cross_ex == None)])
        xs /= sum([mul([core((vu[j]-u[i][j])/cs) for j in range(n1)])
                   for i in range(n) if (cross_ex != None and (i != cross_ex)) or (cross_ex == None)])
    except ZeroDivisionError:
        xs = 100000
    return xs


def normalize(v):
    m = sum(v)/len(v)
    g = sum([(e-m)**2 for e in v])**0.5/len(v)
    return [(e-m)/g for e in v]


def normalize1(v):
    m = 0
    g = sum([(e-m)**2 for e in v])**0.5/len(v)
    return [(e-m)/g for e in v]


def normalize2(v):
    m = max(v)
    return [e/m for e in v]


def normalize3(v):
    m = max(v)
    return [e/m if e > 0 else 0 for e in v]


data = [[float(e) for e in line]
        for line in json.load(open('data/exp/stmp1.json'))]
# print(data)

g = easygraph.GraphTk(x_grid_len=1000)
# x=[]
# y=[]
x, y0, y1, y2 = zip(*data)


def get_est(x, y0, y1, y2):
    y0n1 = normalize1(y0)
    y0e = [x_estimation(y0n1, [(xx,) for xx in x], (e,), 500, CORES[1], i)
           for i, e in enumerate(x)]
    y0e1 = [x_estimation(y0e, [(xx,) for xx in x], (e,), 300, CORES[1])
            for e in range(50, 12001, 50)]
    y1e = [(y-y0e1[i-1])/50 if i > 0 else 0 for i, y in enumerate(y0e1)]
    y1e1 = [x_estimation(y1e, [(xx,) for xx in x], (e,), 300, CORES[1])
            for e in range(50, 12001, 50)]
    y2e = [(y-y1e1[i-1])/50 if i > 0 else 0 for i, y in enumerate(y1e1)]
    y1e2 = [y if y2e[i] > 0 and y > 0 else 0 for i, y in enumerate(y1e1)]
    return normalize2(y1e2)


# y0n, y1n, y2n, y0n1 = normalize(y0), normalize(
#    y1), normalize(y2), normalize1(y0)
# y0e = [x_estimation(y0n1, [(xx,) for xx in x], (e,), 500, CORES[1], i)
#       for i, e in enumerate(x)]
# y0e1 = [x_estimation(y0e, [(xx,) for xx in x], (e,), 300, CORES[1])
#        for e in range(50, 12001, 50)]
#y1e = [(y-y0e1[i-1])/50 if i > 0 else 0 for i, y in enumerate(y0e1)]
# y1e1 = [x_estimation(y1e, [(xx,) for xx in x], (e,), 300, CORES[1])
#        for e in range(50, 12001, 50)]
#y2e = [(y-y1e1[i-1])/50 if i > 0 else 0 for i, y in enumerate(y1e1)]
#y1e2 = [y if y2e[i] > 0 and y > 0 else 0 for i, y in enumerate(y1e1)]
# x0,x1,x2=copy.copy(x)
#x0, y0n = zip(*[e for e in zip(x, y0n) if e[1] > 0])
#x1, y1n = zip(*[e for e in zip(x, y1n) if e[1] > 0])
#x2, y2n = zip(*[e for e in zip(x, y2n) if e[1] > 0])
y1e2n = get_est(x, y0, y1, y2)  # normalize2(y1e2)
#g.addPlotter(easygraph.LinePlotter(g, x, y0n1, 'red', 'метрика количества', 3))
#g.addPlotter(easygraph.LinePlotter(g, x, y0e, 'blue', 'сглаженная метрика количества', 3))
#g.addPlotter(easygraph.LinePlotter(    g, x, y0e1, 'green', 'двукратно сглаженная метрика количества', 3))
#g.addPlotter(easygraph.LinePlotter(    g, x, y1e, 'blue', 'производная двукратно сглаженной метрики количества', 3))
#g.addPlotter(easygraph.LinePlotter(g, x, y1e1, 'red',                                   'сглаженная производная двукратно сглаженной метрики количества', 3))
#g.addPlotter(easygraph.LinePlotter(g, x, y2e, 'red', 'производная сглаженной производной двукратно сглаженной метрики количества', 3))
#g.addPlotter(easygraph.LinePlotter(g, x, y1e2, 'blue',                                   'выборка сглаженной производной двукратно сглаженной метрики количества', 3))
#g.addPlotter(easygraph.LinePlotter(g, x, y1e2n, 'orange',                                 'нормализованная выборка сглаженной производной двукратно сглаженной метрики количества', 3))
raw = {}
raw['y0'], raw['y1'], raw['y2'] = load_data('exp1')
raw['y1n'], raw['y2n'] = normalize(raw['y1']), normalize(raw['y2'])
#g.addPlotter(easygraph.LinePlotter(    g, x, raw['y1'], 'red', 'метрика количества', 2))
g.addPlotter(easygraph.LinePlotter(
    g, x, get_est(x, raw['y0'], raw['y1'], raw['y2']), 'blue', 'оценка', 2))
g.addPlotter(easygraph.LinePlotter(
    g, x, normalize3(raw['y1']), 'red', '1 производная'))
g.addPlotter(easygraph.LinePlotter(
    g, x, normalize3(raw['y2']), 'green', '2 производная'))
#g.addPlotter(easygraph.LinePlotter(g, x0, y0n, 'red', 'метрика количества'))
#g.addPlotter(easygraph.LinePlotter(g, x1, y1n, 'blue', '1 производная'))
#g.addPlotter(easygraph.LinePlotter(g, x2, y2n, 'green', '2 производная'))
g.reGrid(x_mark_list=[i for i in range(0, 12001, 1000)], xmarks=0, grid=True)

# input()
tkinter.mainloop()
