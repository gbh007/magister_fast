package main

import (
	"log"
	"magister_fast/database"
	"os"
	"path"
)

func clear() {
	w, err := os.OpenFile(path.Join(baseDir, "clear.json"), os.O_APPEND|os.O_CREATE|os.O_WRONLY, os.ModePerm)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("start filesearch")
	dir, err := os.Open(baseDir)
	if err != nil {
		log.Fatalln(err)
	}
	if stat, err := dir.Stat(); err != nil || !stat.IsDir() {
		log.Fatalln("is not dir", err)
	}
	names, err := dir.Readdir(0)
	if err != nil {
		log.Fatalln(err)
	}
	log.Printf("finish filesearch, %d files\n", len(names))
	deleted := 0
	moved := 0
	for _, name := range names {
		if !name.IsDir() {
			continue
		}
		fname := path.Join(baseDir, name.Name(), "info.json")
		_, err := os.Lstat(fname)
		if os.IsNotExist(err) {
			continue
		} else if err != nil {
			log.Println(err)
			continue
		}
		inf := &database.ExpInfo{}
		exp, _ := path.Split(fname)
		drop := func() {
			os.RemoveAll(exp)
			log.Printf("deleted %s\n", exp)
			deleted++
		}
		if err := load(fname, &inf); err != nil {
			drop()
			continue
		}
		if !(inf.Fail || inf.Complete) {
			drop()
			continue
		}
		if inf.Fail {
			saveToW(w, &inf)
			os.RemoveAll(exp)
			log.Printf("moved %s\n", exp)
			moved++
		}
	}
	log.Printf("MOVED: %d DELETED: %d\n", moved, deleted)
}
