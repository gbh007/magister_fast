package main

import (
	"log"
	"magister_fast/database"
	"os"
	"path"
	"sort"
	"strconv"
)

func recalcAvg(ename string) {
	db := database.NewDatabase(baseDir)
	db.Load(ename, 0, true)
	dir, err := os.Open(path.Join(baseDir, ename))
	if err != nil {
		log.Println(err)
		return
	}
	files, err := dir.Readdir(0)
	if err != nil {
		log.Println(err)
		return
	}
	steps := make([]int64, 0)
	for _, file := range files {
		if !file.IsDir() {
			continue
		}
		if step, err := strconv.ParseInt(file.Name(), 10, 64); err == nil {
			steps = append(steps, step)
		}
	}
	sort.Slice(steps, func(i, j int) bool { return steps[i] < steps[j] })
	for _, step := range steps {
		db.Load(ename, step, true)
		db.Update(true)
		log.Println("step", step)
	}
	db.SaveAvg(ename)
}
