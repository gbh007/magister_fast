package main

import (
	"fmt"
	"magister_fast/database"
	"math/rand"
	"time"
)

type SPModeling struct {
	db *database.Database
}

func (sm *SPModeling) Make() error {
	return nil
}

func (sm *SPModeling) Before(ename string, remod bool) error {
	rand.Seed(time.Now().Unix())
	sm.db = database.NewDatabase(baseDir)
	if remod {
	} else {
		// добавление генератора монстров
		sm.db.AddMG(&database.EntityGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: -1,
				Start:  1,
				Count:  200,
				XDev:   1000,
				XMean:  0,
				YDev:   1000,
				YMean:  0,
			},
			DamageMean:       50,
			DamageDev:        5,
			MaxHPMean:        60,
			MaxHPDev:         20,
			HPPSMean:         10,
			HPPSDev:          3,
			AttackRadiusMean: 10,
			AttackRadiusDev:  3,
			MaxMoveMean:      10,
			MaxMoveDev:       2,
		})
		// добавление генератора героев
		sm.db.AddHG(&database.EntityGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: -1,
				Start:  1,
				Count:  200,
				XDev:   1000,
				XMean:  0,
				YDev:   1000,
				YMean:  0,
			},
			DamageMean:       50,
			DamageDev:        5,
			MaxHPMean:        60,
			MaxHPDev:         20,
			HPPSMean:         10,
			HPPSDev:          3,
			AttackRadiusMean: 10,
			AttackRadiusDev:  3,
			MaxMoveMean:      10,
			MaxMoveDev:       2,
		})
		sm.db.AddHG(&database.EntityGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: 1,
				Start:  1,
				Count:  2,
				XDev:   1000,
				XMean:  0,
				YDev:   1000,
				YMean:  0,
			},
			DamageMean:       50,
			DamageDev:        5,
			MaxHPMean:        60,
			MaxHPDev:         20,
			HPPSMean:         10,
			HPPSDev:          3,
			AttackRadiusMean: 10,
			AttackRadiusDev:  3,
			MaxMoveMean:      10,
			MaxMoveDev:       2,
		})
		sm.db.World = database.World{
			HeroHPRun:    0.3,
			HeroVamp:     vampire,
			MonsterHPRun: 0,
			MonsterVamp:  vampire,
			LastID:       0,
			Step:         0,
			RandomStart:  0.75,
			RandomLen:    0.5,
		}
	}
	return nil
}
func (sm *SPModeling) Step(i int) error {
	sm.db.Update(false)
	delta := sm.db.World.HeroCount - sm.db.World.MonsterCount
	if delta > 0 {
		sm.db.AddMG(&database.EntityGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: -1,
				Start:  int64(i + 1),
				Count:  int(delta),
				XDev:   1000,
				XMean:  0,
				YDev:   1000,
				YMean:  0,
			},
			DamageMean:       50,
			DamageDev:        5,
			MaxHPMean:        60,
			MaxHPDev:         20,
			HPPSMean:         10,
			HPPSDev:          3,
			AttackRadiusMean: 10,
			AttackRadiusDev:  3,
			MaxMoveMean:      10,
			MaxMoveDev:       2,
		})
	}
	return nil
}
func (sm *SPModeling) Info() string {
	return fmt.Sprintf(
		"step %d monster count %d hero count %d fortress count %d",
		sm.db.World.Step,
		len(sm.db.Monsters),
		len(sm.db.Heroes),
		len(sm.db.Fortresses),
	)
}
func (sm *SPModeling) DB() *database.Database { return sm.db }
