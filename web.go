package main

import (
	"encoding/json"
	"log"
	"magister_fast/database"
	"net/http"
	"os"
	"path"
	"sort"
	"strings"
)

func parseJSON(r *http.Request, data interface{}) error {
	dec := json.NewDecoder(r.Body)
	err := dec.Decode(&data)
	if err != nil {
		log.Println(err)
	}
	return err
}
func writeJSON(w http.ResponseWriter, data interface{}, onlyheader bool) error {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Content-Type", "application/json")
	if onlyheader {
		w.Header().Set("Allow", "POST, OPTIONS")
		return nil
	}
	w.WriteHeader(http.StatusOK)
	enc := json.NewEncoder(w)
	err := enc.Encode(&data)
	if err != nil {
		log.Println(err)
	}
	return err
}

func getInfo(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		writeJSON(w, nil, true)
		return
	}
	req := struct {
		Exp string `json:"exp"`
	}{}
	if err := parseJSON(r, &req); err != nil {
		log.Println(err)
		return
	}
	type SS struct {
		ID   int64  `json:"id"`
		Type string `json:"tp"`
		Born int64  `json:"born"`
		Dead int64  `json:"dead"`
	}
	db := database.NewDatabase(baseDir)
	if err := db.Load(req.Exp, 0, false); err != nil {
		log.Println(err)
		return
	}
	res := make(map[int64]*SS)
	for _, act := range db.ALog {
		if act.AT == database.ABorn {
			res[act.Executor] = &SS{
				ID:   act.Executor,
				Born: act.Step,
			}
			if act.Val == "m" {
				res[act.Executor].Type = "monster"
			}
			if act.Val == "h" {
				res[act.Executor].Type = "hero"
			}
			if act.Val == "f" {
				res[act.Executor].Type = "fortress"
			}
		}
		if act.AT == database.ADead {
			res[act.Executor].Dead = act.Step // может упасть в панику при нарушении работы лога
		}
		if act.AT == database.AKill {
			res[act.Target].Dead = act.Step // может упасть в панику при нарушении работы лога
		}
	}
	ans := make([]*SS, 0, len(res)+5)
	for _, s := range res {
		ans = append(ans, s)
	}
	sort.Slice(ans, func(i, j int) bool { return ans[i].ID < ans[j].ID })
	writeJSON(w, ans, false)
}

func getStat(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		writeJSON(w, nil, true)
		return
	}
	req := struct {
		ID  int64  `json:"id"`
		Exp string `json:"exp"`
	}{}
	if err := parseJSON(r, &req); err != nil {
		log.Println(err)
		return
	}
	db := database.NewDatabase(baseDir)
	if err := db.Load(req.Exp, 0, false); err != nil {
		log.Println(err)
		return
	}
	if res, ok := db.DD.Data[req.ID]; ok {
		writeJSON(w, res, false)
	} else {
		writeJSON(w, struct{}{}, false)
	}
}

func getAvgStat(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		writeJSON(w, nil, true)
		return
	}
	req := struct {
		Exp string `json:"exp"`
	}{}
	if err := parseJSON(r, &req); err != nil {
		log.Println(err)
		return
	}
	db := database.NewDatabase(baseDir)
	if err := db.Load(req.Exp, 0, false); err != nil {
		log.Println(err)
		return
	}
	writeJSON(w, db.Avg, false)

}

func getSnapshot(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		writeJSON(w, nil, true)
		return
	}
	req := struct {
		Step int64  `json:"step"`
		Exp  string `json:"exp"`
	}{}
	if err := parseJSON(r, &req); err != nil {
		log.Println(err)
		return
	}
	db := database.NewDatabase(baseDir)
	db.Load(req.Exp, req.Step, true)
	writeJSON(w, struct {
		Step       int64                `json:"step"`
		Monsters   []*database.Entity   `json:"monsters"`
		Heroes     []*database.Entity   `json:"heroes"`
		Fortresses []*database.Fortress `json:"fortresses"`
	}{
		Monsters:   db.Monsters,
		Heroes:     db.Heroes,
		Fortresses: db.Fortresses,
		Step:       db.World.Step,
	}, false)
}

func getExpList(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		writeJSON(w, nil, true)
		return
	}
	dir, err := os.Open(baseDir)
	if err != nil {
		log.Println(err)
		return
	}
	if stat, err := dir.Stat(); err != nil || !stat.IsDir() {
		return
	}
	names, err := dir.Readdir(0)
	if err != nil {
		log.Println(err)
		return
	}
	res := make([]*database.ExpInfo, 0)
	for _, name := range names {
		if !name.IsDir() {
			continue
		}
		fname := path.Join(baseDir, name.Name(), "info.json")
		_, err := os.Lstat(fname)
		if os.IsNotExist(err) {
			continue
		} else if err != nil {
			log.Println(err)
			continue
		}
		inf := &database.ExpInfo{}
		if load(fname, &inf) == nil {
			res = append(res, inf)
		}
	}
	writeJSON(w, res, false)
}

func getClear(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		writeJSON(w, nil, true)
		return
	}
	req := struct {
		Count int64 `json:"count"`
	}{}
	if err := parseJSON(r, &req); err != nil {
		log.Println(err)
		return
	}
	avg, icount, ilen := analazyCleared(req.Count, "clear.json")
	writeJSON(w, struct {
		Hist  []int64 `json:"hist"`
		Count int64   `json:"count"`
		Ilen  int64   `json:"len"`
	}{
		Hist:  avg,
		Count: icount,
		Ilen:  ilen,
	}, false)
}

func getClearMany(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		writeJSON(w, nil, true)
		return
	}
	req := struct {
		Count int64 `json:"count"`
	}{}
	if err := parseJSON(r, &req); err != nil {
		log.Println(err)
		return
	}
	type Data struct {
		Hist  []int64 `json:"hist"`
		Count int64   `json:"count"`
		Ilen  int64   `json:"len"`
		Name  string  `json:"name"`
	}
	res := make([]*Data, 0)
	dir, err := os.Open(baseDir)
	if err != nil {
		log.Println(err)
		return
	}
	if stat, err := dir.Stat(); err != nil || !stat.IsDir() {
		log.Println("is not dir", err)
		return
	}
	names, err := dir.Readdir(0)
	if err != nil {
		log.Println(err)
		return
	}
	for _, name := range names {
		if name.IsDir() {
			continue
		}
		if strings.Index(name.Name(), ".json") != -1 {
			avg, icount, ilen := analazyCleared(req.Count, name.Name())
			res = append(res, &Data{
				Hist:  avg,
				Count: icount,
				Ilen:  ilen,
				Name:  name.Name()[0 : len(name.Name())-len(".json")],
			})
		}
	}
	writeJSON(w, res, false)
}

func web() {
	http.HandleFunc("/snapshot/get", getSnapshot)
	http.HandleFunc("/stat/get", getStat)
	http.HandleFunc("/avg/get", getAvgStat)
	http.HandleFunc("/info/get", getInfo)
	http.HandleFunc("/exp/get", getExpList)
	http.HandleFunc("/hist/get", getClear)
	http.HandleFunc("/hist/get/many", getClearMany)
	http.Handle("/", http.StripPrefix("/", http.FileServer(http.Dir("./static/"))))
	log.Println("start web server")
	log.Println(http.ListenAndServe(":"+port, nil))
	log.Println("finish web server")
}
