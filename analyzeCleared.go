package main

import (
	"bufio"
	"encoding/json"
	"log"
	"magister_fast/database"
	"os"
	"path"
)

func analazyCleared(icount int64, name string) ([]int64, int64, int64) {
	var ilen = 12000 / icount
	r, err := os.Open(path.Join(baseDir, name))
	defer r.Close()
	if err != nil {
		log.Println(err)
	}
	scan := bufio.NewScanner(r)
	inf := &database.ExpInfo{}
	avg := make([]int64, icount+1)
	for scan.Scan() {
		if err = json.Unmarshal(scan.Bytes(), &inf); err != nil {
			log.Println(err)
			continue
		}
		if inf.LastStep/ilen > icount {
			avg[icount]++
		} else {
			avg[inf.LastStep/ilen]++
		}
	}
	return avg, icount, ilen
}
