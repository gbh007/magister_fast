package main

import (
	"log"
	"magister_fast/database"
	"os"
	"path"
)

func scan() {
	log.Println("start search")
	dir, err := os.Open(baseDir)
	if err != nil {
		log.Fatalln(err)
	}
	if stat, err := dir.Stat(); err != nil || !stat.IsDir() {
		log.Fatalln("is not dir", err)
	}
	names, err := dir.Readdir(0)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("finish search")
	for _, name := range names {
		if !name.IsDir() {
			continue
		}
		fname := path.Join(baseDir, name.Name(), "info.json")
		_, err := os.Lstat(fname)
		if os.IsNotExist(err) {
			continue
		} else if err != nil {
			log.Println(err)
			continue
		}
		inf := &database.ExpInfo{}
		load(fname, &inf)
		status := ""
		if inf.Fail {
			status = "FAIL"
		} else if inf.Complete {
			status = "FINISH"
		}
		log.Printf("%-30s -> %8s = LS: %8d | MC: %8d | HC: %8d | FC: %8d", inf.Name, status, inf.LastStep, inf.MonsterCount, inf.HeroCount, inf.FortressCount)
	}
}
