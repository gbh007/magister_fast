package main

import (
	"flag"
	"fmt"
	"log"
	"magister_fast/database"
	"math"
	"os"
	"runtime/pprof"
	"sync"
	"time"
)

type runner interface {
	Before(ename string, remod bool) error
	Step(i int) error
	Make() error
	Info() string
	DB() *database.Database
}

var (
	port          = "8080"
	iterCount     = 12000
	randIterCount = 100
	baseDir       = "data"
	vampire       = 0.0
	precision     = 100
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	file, err := os.Create("cpu.prof")
	if err != nil {
		log.Fatalln(err)
	}
	if err := pprof.StartCPUProfile(file); err != nil {
		log.Fatalln(err)
	}
	defer pprof.StopCPUProfile()
	many := flag.Bool("m", false, "run many")
	remod := flag.Bool("remod", false, "remodeling experiment")
	wb := flag.Bool("w", false, "run web server")
	mod := flag.String("mod", "", "modeling mode: simple, random, gen, if, super, sp")
	sc := flag.Bool("sc", false, "scan data")
	clr := flag.Bool("cl", false, "clear data")
	reavg := flag.Bool("reavg", false, "remodeling avg")
	flag.Float64Var(&vampire, "vmp", 0.0, "vampire coef")
	flag.StringVar(&port, "p", "8080", "web port")
	flag.IntVar(&iterCount, "i", 12000, "modeling iteration count")
	flag.IntVar(&randIterCount, "ri", math.MaxInt64, "many modeling iteration count")
	flag.IntVar(&precision, "prec", 100, "save interval")
	experimentName := flag.String("e", "exp", "experiment name")
	flag.StringVar(&baseDir, "d", "data", "data dir")
	flag.Parse()
	if *sc {
		scan()
		return
	}
	if *clr {
		clear()
		return
	}
	if *reavg {
		recalcAvg(*experimentName)
		return
	}
	wg := &sync.WaitGroup{}
	if *wb {
		wg.Add(1)
		go func() {
			web()
			wg.Done()
		}()
	}
	var modeler runner
	switch *mod {
	case "simple":
		modeler = &SimpleModeling{}
	case "random":
		modeler = &RandomModeling{}
	case "gen":
		modeler = &GenModeling{}
	case "if":
		modeler = &IfModeling{}
	case "super":
		modeler = &SuperModeling{}
	case "sp":
		modeler = &SPModeling{}
	}
	if modeler != nil {
		modeler.Make()
		if *remod {
			ename := *experimentName
			modeler.Before(ename, *remod)
			modeler.DB().Load(ename, 0, true)
			modeler.DB().ClearDir(ename)
			modeler.DB().Save(ename, true)
			log.Println(ename, "START")
			for i := 1; i <= iterCount; i++ {
				modeler.DB().Step()
				modeler.DB().Update(false)
				modeler.Step(i)
				if i%precision == 0 {
					modeler.DB().Update(true)
					log.Println(modeler.Info())
					modeler.DB().Save(ename, true)
				}
			}
			log.Println(ename, "FINISH")
			modeler.DB().Info.Complete = true
			modeler.DB().Save(ename, true)
		} else if *many {
			log.Println("start many")
			for iter := 0; iter < randIterCount; iter++ {
				ename := fmt.Sprintf("%s - %s - %s %d", *experimentName, *mod, time.Now().Format("2006-01-02 15-04-05"), iter)
				modeler.Before(ename, *remod)
				modeler.DB().ClearDir(ename)
				modeler.DB().Save(ename, true)
				log.Println(ename, "START")
				for i := 1; i <= iterCount; i++ {
					modeler.DB().Step()
					modeler.DB().Update(false)
					if !modeler.DB().Check() {
						modeler.DB().Info.Fail = true
						modeler.DB().Info.Complete = true
						modeler.DB().Save(ename, true)
						log.Println(ename, "FAIL")
						break
					}
					modeler.Step(i)
					if i%precision == 0 {
						modeler.DB().Update(true)
						log.Println(modeler.Info())
						modeler.DB().Save(ename, true)
					}
				}
				if !modeler.DB().Info.Fail {
					modeler.DB().Info.Complete = true
					modeler.DB().Save(ename, true)
					log.Println(ename, "SUCCESS")
				}
				if iter%100 == 0 {
					clear()
				}
			}
			log.Println("finish many")
			clear()
		} else {
			ename := *experimentName
			modeler.Before(ename, *remod)
			modeler.DB().ClearDir(ename)
			modeler.DB().Save(ename, true)
			log.Println(ename, "START")
			for i := 1; i <= iterCount; i++ {
				modeler.DB().Step()
				modeler.DB().Update(false)
				modeler.Step(i)
				if i%precision == 0 {
					modeler.DB().Update(true)
					log.Println(modeler.Info())
					modeler.DB().Save(ename, true)
				}
			}
			log.Println(ename, "FINISH")
			modeler.DB().Info.Complete = true
			modeler.DB().Save(ename, true)
		}
	}
	wg.Wait()
}
