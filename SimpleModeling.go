package main

import (
	"fmt"
	"magister_fast/database"
	"math/rand"
	"time"
)

type SimpleModeling struct {
	db *database.Database
}

func (sm *SimpleModeling) Make() error {
	return nil
}

func (sm *SimpleModeling) Before(ename string, remod bool) error {
	rand.Seed(time.Now().Unix())
	sm.db = database.NewDatabase(baseDir)
	if remod {
	} else {
		// добавление генератора монстров
		sm.db.AddMG(&database.EntityGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: 50,
				Start:  1,
				Count:  20,
				XDev:   1000,
				XMean:  0,
				YDev:   1000,
				YMean:  0,
			},
			DamageMean:       40,
			DamageDev:        5,
			MaxHPMean:        100,
			MaxHPDev:         20,
			HPPSMean:         10,
			HPPSDev:          3,
			AttackRadiusMean: 10,
			AttackRadiusDev:  3,
			MaxMoveMean:      5,
			MaxMoveDev:       1,
		})
		// добавление генератора героев
		sm.db.AddHG(&database.EntityGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: 50,
				Start:  1,
				Count:  20,
				XDev:   1000,
				XMean:  0,
				YDev:   1000,
				YMean:  0,
			},
			DamageMean:       50,
			DamageDev:        5,
			MaxHPMean:        60,
			MaxHPDev:         20,
			HPPSMean:         10,
			HPPSDev:          3,
			AttackRadiusMean: 10,
			AttackRadiusDev:  3,
			MaxMoveMean:      10,
			MaxMoveDev:       2,
		})
		// добавление генератора крепостей
		sm.db.AddFG(&database.FortressGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: 100,
				Start:  1,
				Count:  5,
				XDev:   100,
				XMean:  0,
				YDev:   100,
				YMean:  0,
			},
			DamageMean:       50,
			DamageDev:        10,
			AttackRadiusMean: 20,
			AttackRadiusDev:  5,
			LiveTimeStart:    500,
			LiveTimeLen:      500,
		})
		sm.db.World = database.World{
			HeroHPRun:    0.3,
			HeroVamp:     vampire,
			MonsterHPRun: 0.15,
			MonsterVamp:  vampire,
			LastID:       0,
			Step:         0,
			RandomStart:  0.75,
			RandomLen:    0.5,
		}
	}
	return nil
}
func (sm *SimpleModeling) Step(i int) error { return nil }
func (sm *SimpleModeling) Info() string {
	return fmt.Sprintf(
		"step %d monster count %d hero count %d fortress count %d",
		sm.db.World.Step,
		len(sm.db.Monsters),
		len(sm.db.Heroes),
		len(sm.db.Fortresses),
	)
}
func (sm *SimpleModeling) DB() *database.Database { return sm.db }
