package database

type akey int8

// ключи селектора действий
const (
	AKill akey = iota
	AAttack
	ABorn
	ADead
	ABuffed
)

// Act структура для действия
type Act struct {
	Step     int64       `json:"s"`
	Executor int64       `json:"e"`
	Target   int64       `json:"t"`
	AT       akey        `json:"a"`
	Val      interface{} `json:"v"` // дополнительное значение
}

// ActionLog структура для логирования действий
type ActionLog []Act

// Attack логирует атаку
func (a *ActionLog) Attack(step, ex, tar int64, val float64) {
	// на данный момент игнорируется
	*a = append(*a, Act{
		Step:     step,
		Executor: ex,
		Target:   tar,
		AT:       AAttack,
		Val:      dround(val),
	})
}

// Kill логирует убийство
func (a *ActionLog) Kill(step, ex, tar int64) {
	*a = append(*a, Act{
		Step:     step,
		Executor: ex,
		Target:   tar,
		AT:       AKill,
	})
}

// Buff логирует усиление
func (a *ActionLog) Buff(step, ex, tar int64) {
	*a = append(*a, Act{
		Step:     step,
		Executor: ex,
		Target:   tar,
		AT:       ABuffed,
	})
}

// Born логирует рождение
func (a *ActionLog) Born(step, ex int64, tp string) {
	*a = append(*a, Act{
		Step:     step,
		Executor: ex,
		Target:   -1,
		AT:       ABorn,
		Val:      tp,
	})
}

// Dead логирует смерть (если не убит)
func (a *ActionLog) Dead(step, ex int64) {
	*a = append(*a, Act{
		Step:     step,
		Executor: ex,
		Target:   -1,
		AT:       ADead,
	})
}
