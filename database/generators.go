package database

import (
	"math/rand"
)

// SimpleGenerator структура для простого генератора
type SimpleGenerator struct {
	Start  int64   `json:"start"`
	Period int64   `json:"period"`
	Count  int     `json:"count"`
	XMean  float64 `json:"x_mean"`
	XDev   float64 `json:"x_dev"`
	YMean  float64 `json:"y_mean"`
	YDev   float64 `json:"y_dev"`
}

// EntityGenerator генератор для существ
type EntityGenerator struct {
	SimpleGenerator
	MaxHPMean        float64 `json:"max_hp_mean"`
	MaxHPDev         float64 `json:"max_hp_dev"`
	HPPSMean         float64 `json:"hpps_mean"`
	HPPSDev          float64 `json:"hpps_dev"`
	DamageMean       float64 `json:"dmg_mean"`
	DamageDev        float64 `json:"dmg_dev"`
	AttackRadiusMean float64 `json:"attack_radius_mean"`
	AttackRadiusDev  float64 `json:"attack_radius_dev"`
	MaxMoveMean      float64 `json:"max_move_mean"`
	MaxMoveDev       float64 `json:"max_move_dev"`
}

// Gen генерирует массив существ по параметрам генератора
func (eg *EntityGenerator) Gen(startID int64) (res []*Entity) {
	for i := 0; i < eg.Count; i++ {
		res = append(res, &Entity{
			ID: startID + int64(i),
			Cord: Cord{
				X: rand.NormFloat64()*eg.XDev + eg.XMean,
				Y: rand.NormFloat64()*eg.YDev + eg.YMean,
			},
			Damage:       rand.NormFloat64()*eg.DamageDev + eg.DamageMean,
			HP:           rand.NormFloat64()*eg.MaxHPDev + eg.MaxHPMean,
			HPMax:        rand.NormFloat64()*eg.MaxHPDev + eg.MaxHPMean,
			HPPerStep:    rand.NormFloat64()*eg.HPPSDev + eg.HPPSMean,
			AttackRadius: rand.NormFloat64()*eg.AttackRadiusDev + eg.AttackRadiusMean,
			MaxMove:      rand.NormFloat64()*eg.MaxMoveDev + eg.MaxMoveMean,
		})
	}
	return
}

// FortressGenerator генератор для крепостей
type FortressGenerator struct {
	SimpleGenerator
	DamageMean       float64 `json:"dmg_mean"`
	DamageDev        float64 `json:"dmg_dev"`
	AttackRadiusMean float64 `json:"attack_radius_mean"`
	AttackRadiusDev  float64 `json:"attack_radius_dev"`
	LiveTimeStart    int64   `json:"live_time_start"`
	LiveTimeLen      int64   `json:"live_time_len"`
}

// Gen генерирует массив крепостей по параметрам генератора
func (fg *FortressGenerator) Gen(startID int64) (res []*Fortress) {
	for i := 0; i < fg.Count; i++ {
		res = append(res, &Fortress{
			ID: startID + int64(i),
			Cord: Cord{
				X: rand.NormFloat64()*fg.XDev + fg.XMean,
				Y: rand.NormFloat64()*fg.YDev + fg.YMean,
			},
			LiveTime:     rand.Int63n(fg.LiveTimeLen) + fg.LiveTimeStart,
			Damage:       rand.NormFloat64()*fg.DamageDev + fg.DamageMean,
			AttackRadius: rand.NormFloat64()*fg.AttackRadiusDev + fg.AttackRadiusMean,
		})
	}
	return
}
