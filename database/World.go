package database

import "math/rand"

// World структура с глобальными данными
type World struct {
	Step          int64   `json:"step"`
	HeroHPRun     float64 `json:"hero_hp_run"`
	HeroVamp      float64 `json:"hero_vamp"`
	MonsterHPRun  float64 `json:"monster_hp_run"`
	MonsterVamp   float64 `json:"monster_vamp"`
	LastID        int64   `json:"last_id"`
	MonsterCount  int64   `json:"monster_count"`
	HeroCount     int64   `json:"hero_count"`
	FortressCount int64   `json:"fortress_count"`
	RandomStart   float64 `json:"rand_st"`
	RandomLen     float64 `json:"rand_len"`
}

// GetRand возвращает рандомное значение в заданом диапазоне
func (w *World) GetRand() float64 {
	return w.RandomStart + rand.Float64()*w.RandomLen
}
