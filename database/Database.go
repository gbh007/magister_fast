package database

import (
	"encoding/json"
	"log"
	"math"
	"os"
	"path"
	"strconv"
)

// ExpInfo информация об эксперименте (хранится в корне эксперимента)
type ExpInfo struct {
	LastStep      int64  `json:"last_step"`
	Complete      bool   `json:"complete"`
	Fail          bool   `json:"fail"`
	MonsterCount  int64  `json:"monster_count"`
	HeroCount     int64  `json:"hero_count"`
	FortressCount int64  `json:"fortress_count"`
	Name          string `json:"name"`
}

// Database структура для работы с базой
type Database struct {
	Dir        string
	Monsters   []*Entity
	Heroes     []*Entity
	Fortresses []*Fortress
	MG         []*EntityGenerator
	HG         []*EntityGenerator
	FG         []*FortressGenerator
	World      World
	needClear  bool
	Info       ExpInfo
	DD         *DiffData
	ALog       ActionLog
	Avg        AvgStat
}

// NewDatabase создание нового объекта базы
func NewDatabase(dir string) *Database {
	return &Database{
		Dir:        dir,
		Monsters:   make([]*Entity, 0),
		Heroes:     make([]*Entity, 0),
		Fortresses: make([]*Fortress, 0),
		MG:         make([]*EntityGenerator, 0),
		HG:         make([]*EntityGenerator, 0),
		FG:         make([]*FortressGenerator, 0),
		World:      World{},
		DD:         newDD(),
		Avg:        newAvgStat(),
	}
}

// NeedClear при вызове указвает что нужна отчистка данных (умер монстр, или вышел генератор)
func (db *Database) NeedClear() {
	db.needClear = true
}

// DeadClear удаляет из базы мертвых героев и неработающие генераторы (время активации которых прошло)
func (db *Database) DeadClear() {
	if !db.needClear {
		return
	}
	tmpMonsters := make([]*Entity, 0)
	for _, monster := range db.Monsters {
		if monster.HP > 0 {
			tmpMonsters = append(tmpMonsters, monster)
		}
	}
	db.Monsters = tmpMonsters
	tmpHeroes := make([]*Entity, 0)
	for _, hero := range db.Heroes {
		if hero.HP > 0 {
			tmpHeroes = append(tmpHeroes, hero)
		}
	}
	db.Heroes = tmpHeroes
	tmpFortresses := make([]*Fortress, 0)
	for _, fortress := range db.Fortresses {
		if fortress.LiveTime > 0 {
			tmpFortresses = append(tmpFortresses, fortress)
		}
	}
	db.Fortresses = tmpFortresses
	tmpMG := make([]*EntityGenerator, 0)
	for _, mg := range db.MG {
		if mg.Start >= db.World.Step {
			tmpMG = append(tmpMG, mg)
		}
	}
	db.MG = tmpMG
	tmpHG := make([]*EntityGenerator, 0)
	for _, hg := range db.HG {
		if hg.Start >= db.World.Step {
			tmpHG = append(tmpHG, hg)
		}
	}
	db.HG = tmpHG
	tmpFG := make([]*FortressGenerator, 0)
	for _, fg := range db.FG {
		if fg.Start >= db.World.Step {
			tmpFG = append(tmpFG, fg)
		}
	}
	db.FG = tmpFG
	db.needClear = false
}

// AddMonster добавляет в базу монстра
func (db *Database) AddMonster(m ...*Entity) {
	for _, ent := range m {
		db.ALog.Born(db.World.Step, ent.ID, "m")
	}
	db.Monsters = append(db.Monsters, m...)
}

// AddHero добавляет в базу героя
func (db *Database) AddHero(h ...*Entity) {
	for _, ent := range h {
		db.ALog.Born(db.World.Step, ent.ID, "h")
	}
	db.Heroes = append(db.Heroes, h...)
}

// AddFortress добавляет в базу крепость
func (db *Database) AddFortress(f ...*Fortress) {
	for _, ent := range f {
		db.ALog.Born(db.World.Step, ent.ID, "f")
	}
	db.Fortresses = append(db.Fortresses, f...)
}

// AddMG добавляет в базу генератор монстров
func (db *Database) AddMG(mg ...*EntityGenerator) { db.MG = append(db.MG, mg...) }

// AddHG добавляет в базу генератор героев
func (db *Database) AddHG(hg ...*EntityGenerator) { db.HG = append(db.HG, hg...) }

// AddFG добавляет в базу генератор крепостей
func (db *Database) AddFG(fg ...*FortressGenerator) { db.FG = append(db.FG, fg...) }

func load(filename string, data interface{}) error {
	if file, err := os.Open(filename); err == nil {
		err := json.NewDecoder(file).Decode(&data)
		file.Close()
		if err != nil {
			log.Println(err)
			return err
		}
	} else {
		log.Println(err)
		return err
	}
	return nil
}

// ClearDir отчищает папку с файлами
func (db *Database) ClearDir(exp string) error {
	return os.RemoveAll(path.Join(db.Dir, exp))
}

// Load загружает данные эксперимента
func (db *Database) Load(exp string, step int64, snapshot bool) error {
	if !snapshot {
		// загрузка данных о эксперименте
		db.Info = ExpInfo{}
		if err := load(path.Join(db.Dir, exp, "info.json"), &db.Info); err != nil {
			log.Println(err)
			return err
		}
		// загрузка данных о изменениях
		db.DD = newDD()
		if err := load(path.Join(db.Dir, exp, "diff.json"), &db.DD.Data); err != nil {
			log.Println(err)
			return err
		}
		// загрузка лога действий
		if err := load(path.Join(db.Dir, exp, "alog.json"), &db.ALog); err != nil {
			log.Println(err)
			return err
		}
		// загрузка статистики
		if err := load(path.Join(db.Dir, exp, "stat.json"), &db.Avg); err != nil {
			log.Println(err)
			return err
		}
	} else {
		dir := path.Join(db.Dir, exp, strconv.FormatInt(step, 10))
		if _, err := os.Lstat(dir); os.IsNotExist(err) {
			log.Println(err)
			return err
		}
		// загрузка данных о мире
		db.World = World{}
		if err := load(path.Join(dir, "world.json"), &db.World); err != nil {
			log.Println(err)
			return err
		}
		// загрузка данных о монстрах
		db.Monsters = make([]*Entity, 0)
		if err := load(path.Join(dir, "monster.json"), &db.Monsters); err != nil {
			log.Println(err)
			return err
		}
		// загрузка данных о героях
		db.Heroes = make([]*Entity, 0)
		if err := load(path.Join(dir, "hero.json"), &db.Heroes); err != nil {
			log.Println(err)
			return err
		}
		// загрузка данных о крепостях
		db.Fortresses = make([]*Fortress, 0)
		if err := load(path.Join(dir, "fortress.json"), &db.Fortresses); err != nil {
			log.Println(err)
			return err
		}
		// загрузка данных о генераторах монстров
		db.MG = make([]*EntityGenerator, 0)
		if err := load(path.Join(dir, "mg.json"), &db.MG); err != nil {
			log.Println(err)
			return err
		}
		// загрузка данных о генераторах героев
		db.HG = make([]*EntityGenerator, 0)
		if err := load(path.Join(dir, "hg.json"), &db.HG); err != nil {
			log.Println(err)
			return err
		}
		// загрузка данных о генераторах крепостей
		db.FG = make([]*FortressGenerator, 0)
		if err := load(path.Join(dir, "fg.json"), &db.FG); err != nil {
			log.Println(err)
			return err
		}
	}
	return nil
}

func save(filename string, data interface{}) error {
	if file, err := os.Create(filename); err == nil {
		err := json.NewEncoder(file).Encode(&data)
		file.Close()
		if err != nil {
			log.Println(err)
			return err
		}
	} else {
		log.Println(err)
		return err
	}
	return nil
}

// Update обновляет состояние
func (db *Database) Update(full bool) {
	db.Info.LastStep = db.World.Step
	db.World.MonsterCount = int64(len(db.Monsters))
	db.World.HeroCount = int64(len(db.Heroes))
	db.World.FortressCount = int64(len(db.Fortresses))
	db.Info.MonsterCount = db.World.MonsterCount
	db.Info.HeroCount = db.World.HeroCount
	db.Info.FortressCount = db.World.FortressCount
	if full {
		db.DD.update(db)
		db.Avg.Calc(db)
	}
}

// SaveAvg сохраняет данные статистики эксперимента
func (db *Database) SaveAvg(exp string) error {
	expDir := path.Join(db.Dir, exp)
	if _, err := os.Lstat(expDir); os.IsNotExist(err) {
		if err := os.MkdirAll(expDir, os.ModePerm); err != nil {
			log.Println(err)
			return err
		}
	}
	// выгрузка статистики
	if err := save(path.Join(db.Dir, exp, "stat.json"), &db.Avg); err != nil {
		log.Println(err)
		return err
	}
	return nil
}

// Save сохраняет данные эксперимента
func (db *Database) Save(exp string, snapshot bool) error {
	expDir := path.Join(db.Dir, exp)
	if _, err := os.Lstat(expDir); os.IsNotExist(err) {
		if err := os.MkdirAll(expDir, os.ModePerm); err != nil {
			log.Println(err)
			return err
		}
	}
	db.Update(true)
	db.Info.Name = exp
	// выгрузка данных о эксперименте
	if err := save(path.Join(db.Dir, exp, "info.json"), &db.Info); err != nil {
		log.Println(err)
		return err
	}
	// выгрузка данных об изменениях
	if err := save(path.Join(db.Dir, exp, "diff.json"), &db.DD.Data); err != nil {
		log.Println(err)
		return err
	}
	// выгрузка лога действий
	if err := save(path.Join(db.Dir, exp, "alog.json"), &db.ALog); err != nil {
		log.Println(err)
		return err
	}
	// выгрузка статистики
	if err := save(path.Join(db.Dir, exp, "stat.json"), &db.Avg); err != nil {
		log.Println(err)
		return err
	}
	if snapshot {
		dir := path.Join(db.Dir, exp, strconv.FormatInt(db.World.Step, 10))
		if _, err := os.Lstat(dir); os.IsNotExist(err) {
			if err := os.MkdirAll(dir, os.ModePerm); err != nil {
				log.Println(err)
				return err
			}
		}
		// выгрузка данных о мире
		if err := save(path.Join(dir, "world.json"), &db.World); err != nil {
			log.Println(err)
			return err
		}
		// выгрузка данных о монстрах
		if err := save(path.Join(dir, "monster.json"), &db.Monsters); err != nil {
			log.Println(err)
			return err
		}
		// выгрузка данных о героях
		if err := save(path.Join(dir, "hero.json"), &db.Heroes); err != nil {
			log.Println(err)
			return err
		}
		// выгрузка данных о крепостях
		if err := save(path.Join(dir, "fortress.json"), &db.Fortresses); err != nil {
			log.Println(err)
			return err
		}
		// выгрузка данных о генераторах монстров
		if err := save(path.Join(dir, "mg.json"), &db.MG); err != nil {
			log.Println(err)
			return err
		}
		// выгрузка данных о генераторах героев
		if err := save(path.Join(dir, "hg.json"), &db.HG); err != nil {
			log.Println(err)
			return err
		}
		// выгрузка данных о генераторах крепостей
		if err := save(path.Join(dir, "fg.json"), &db.FG); err != nil {
			log.Println(err)
			return err
		}
	}
	return nil
}

// Check возвращает истину если мира в норме иначе лож
func (db *Database) Check() bool {
	hc := float64(db.World.HeroCount)
	mc := float64(db.World.MonsterCount)
	stat := mc/hc + hc/mc
	if math.IsInf(stat, 0) {
		stat = 999
	}
	if math.IsNaN(stat) {
		stat = 0
	}
	if db.World.Step > 500 &&
		hc+mc > 50 &&
		stat > 3 {
		return false
	}
	return true
}
