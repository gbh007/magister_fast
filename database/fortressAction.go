package database

func fortressAction(db *Database) {
	for _, fortress := range db.Fortresses {
		// атака слабейшего (минимум хп) монстра в зоне поражения
		var id int
		id = -1
		var coef float64
		for ind, monster := range db.Monsters {
			if radius(monster, fortress) < fortress.AttackRadius {
				if id == -1 || (monster.HP < coef && monster.HP > 0) {
					id = ind
					coef = monster.HP
				}
			}
		}
		if id != -1 {
			monster := db.Monsters[id]
			dmg := fortress.Damage * db.World.GetRand()
			monster.HP -= dmg
			db.ALog.Attack(db.World.Step, fortress.ID, monster.ID, dmg)
			if monster.HP <= 0 {
				r := 0.0
				id = -1
				for i, hero := range db.Heroes {
					if r1 := radius(hero, fortress); (hero.HP > 0) && (id == -1 || r1 < r) {
						r = r1
						id = i
					}
				}
				if id != -1 {
					hero := db.Heroes[id]
					hero.HP += monster.HPMax * db.World.HeroVamp * db.World.GetRand()
					hero.HPMax += monster.HPMax * db.World.HeroVamp * db.World.GetRand()
					hero.HPPerStep += monster.HPPerStep * db.World.HeroVamp * db.World.GetRand()
					hero.Damage += monster.Damage * db.World.HeroVamp * db.World.GetRand()
					db.ALog.Buff(db.World.Step, fortress.ID, hero.ID)
				}
				fortress.DefeatedCount++
				fortress.DefeatedLevel += weakMax(monster)
				db.ALog.Kill(db.World.Step, fortress.ID, monster.ID)
				db.NeedClear()
			}
			continue
		}
	}
}
