package database

import (
	"math"
)

// AvgStat структура для подсчета статистики
type AvgStat map[string][]SD

func newAvgStat() AvgStat {
	return make(map[string][]SD)
}

// Calc обновляет статистику
func (a *AvgStat) Calc(db *Database) {
	if db.World.Step < 1 {
		return
	}
	a.calcEntityCount(db)
	a.calcStat(db)
	//a.calcMonster(db)
	//a.calcHero(db)
}

func (a *AvgStat) calcStat(db *Database) {
	hc := float64(db.World.HeroCount)
	mc := float64(db.World.MonsterCount)
	if mc < 1 || hc < 1 {
		return
	}
	stat := dsafe(mc/hc-1) + dsafe(hc/mc-1)
	a.addStat("мера количества", db.World.Step, dround(stat))
	type Stata struct {
		min, max, val float64
	}
	avgMCHP := &Stata{min: 999999}
	avgMCDmg := &Stata{min: 999999}
	for _, monster := range db.Monsters {
		avgMCHP.val += monster.HPMax
		avgMCHP.min = math.Min(monster.HPMax, avgMCHP.min)
		avgMCHP.max = math.Max(monster.HPMax, avgMCHP.max)
		avgMCDmg.val += monster.Damage
		avgMCDmg.min = math.Min(monster.Damage, avgMCDmg.min)
		avgMCDmg.max = math.Max(monster.Damage, avgMCDmg.max)
	}
	if mc > 0 {
		avgMCHP.val /= mc
		avgMCDmg.val /= mc
	}
	avgHCHP := &Stata{min: 999999}
	avgHCDmg := &Stata{min: 999999}
	for _, hero := range db.Heroes {
		avgHCHP.val += hero.HPMax
		avgHCHP.min = math.Min(hero.HPMax, avgHCHP.min)
		avgHCHP.max = math.Max(hero.HPMax, avgHCHP.max)
		avgHCDmg.val += hero.Damage
		avgHCDmg.min = math.Min(hero.Damage, avgHCDmg.min)
		avgHCDmg.max = math.Max(hero.Damage, avgHCDmg.max)
	}
	if hc > 0 {
		avgHCHP.val /= hc
		avgHCDmg.val /= hc
	}
	a.addStat("мера силы", db.World.Step, dround(
		dsafe(avgHCDmg.val/avgMCDmg.val-1)+
			dsafe(avgMCDmg.val/avgHCDmg.val-1)+
			dsafe(avgHCHP.val/avgMCHP.val-1)+
			dsafe(avgMCHP.val/avgHCHP.val-1),
	))
	a.addStat("мера превосходства", db.World.Step, dround(
		dsafe(avgHCDmg.max/avgMCDmg.min-1)+
			dsafe(avgMCDmg.max/avgHCDmg.min-1)+
			dsafe(avgHCHP.max/avgMCHP.min-1)+
			dsafe(avgMCHP.max/avgHCHP.min-1),
	))
	if arr, ok := (*a)["мера количества"]; ok && len(arr) > 1 {
		a.addStat("dFp/dt", db.World.Step, (arr[len(arr)-2].Val.(float64)-arr[len(arr)-1].Val.(float64))/float64(arr[len(arr)-2].Step-arr[len(arr)-1].Step))
	}
	if arr, ok := (*a)["dFp/dt"]; ok && len(arr) > 1 {
		a.addStat("2dFp/dt^2", db.World.Step, (arr[len(arr)-2].Val.(float64)-arr[len(arr)-1].Val.(float64))/float64(arr[len(arr)-2].Step-arr[len(arr)-1].Step))
	}
}

func (a *AvgStat) calcEntityCount(db *Database) {
	a.addStat("количество монстров", db.World.Step, len(db.Monsters))
	a.addStat("количество героев", db.World.Step, len(db.Heroes))
	a.addStat("количество крепостей", db.World.Step, len(db.Fortresses))
}

func (a *AvgStat) calcMonster(db *Database) {
	count := float64(len(db.Monsters))
	avgHP := 0.0
	avgMaxHP := 0.0
	avgHPS := 0.0
	avgAR := 0.0
	avgDmg := 0.0
	avgDefC := 0.0
	avgDefLv := 0.0
	for _, monster := range db.Monsters {
		avgHP += monster.HP
		avgMaxHP += monster.HPMax
		avgHPS += monster.HPPerStep
		avgAR += monster.AttackRadius
		avgDmg += monster.Damage
		avgDefC += float64(monster.DefeatedCount)
		avgDefLv += monster.DefeatedLevel
	}
	if count > 0 {
		avgHP /= count
		avgMaxHP /= count
		avgHPS /= count
		avgAR /= count
		avgDmg /= count
		avgDefC /= count
		avgDefLv /= count
	}
	a.addStat("monster_avg_hp", db.World.Step, dround(avgHP))
	a.addStat("monster_avg_hp_max", db.World.Step, dround(avgMaxHP))
	a.addStat("monster_avg_hps", db.World.Step, dround(avgHPS))
	a.addStat("monster_avg_ar", db.World.Step, dround(avgAR))
	a.addStat("monster_avg_dmg", db.World.Step, dround(avgDmg))
	a.addStat("monster_avg_def_count", db.World.Step, dround(avgDefC))
	a.addStat("monster_avg_def_lv", db.World.Step, dround(avgDefLv))
}

func (a *AvgStat) calcHero(db *Database) {
	count := float64(len(db.Heroes))
	avgHP := 0.0
	avgMaxHP := 0.0
	avgHPS := 0.0
	avgAR := 0.0
	avgDmg := 0.0
	avgDefC := 0.0
	avgDefLv := 0.0
	for _, hero := range db.Heroes {
		avgHP += hero.HP
		avgMaxHP += hero.HPMax
		avgHPS += hero.HPPerStep
		avgAR += hero.AttackRadius
		avgDmg += hero.Damage
		avgDefC += float64(hero.DefeatedCount)
		avgDefLv += hero.DefeatedLevel
	}
	if count > 0 {
		avgHP /= count
		avgMaxHP /= count
		avgHPS /= count
		avgAR /= count
		avgDmg /= count
		avgDefC /= count
		avgDefLv /= count
	}
	a.addStat("hero_avg_hp", db.World.Step, dround(avgHP))
	a.addStat("hero_avg_hp_max", db.World.Step, dround(avgMaxHP))
	a.addStat("hero_avg_hps", db.World.Step, dround(avgHPS))
	a.addStat("hero_avg_ar", db.World.Step, dround(avgAR))
	a.addStat("hero_avg_dmg", db.World.Step, dround(avgDmg))
	a.addStat("hero_avg_def_count", db.World.Step, dround(avgDefC))
	a.addStat("hero_avg_def_lv", db.World.Step, dround(avgDefLv))
}

func (a AvgStat) addStat(key string, step int64, val interface{}) {
	if _, ok := a[key]; !ok {
		a[key] = make([]SD, 0)
	}
	arr := a[key]
	if len(arr) == 0 || arr[len(arr)-1].Val != val {
		a[key] = append(arr, SD{Step: step, Val: val})
	}
}
