package database

import "math"

// Cord реализация координат
type Cord struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}

// GetX возвращает X координату
func (c *Cord) GetX() float64 { return c.X }

// GetY возврашет Y координату
func (c *Cord) GetY() float64 { return c.Y }

func (c *Cord) fix() bool {
	fix := false
	if math.IsNaN(c.X) {
		fix = true
		c.X = 0
	}
	if math.IsNaN(c.Y) {
		fix = true
		c.Y = 0
	}
	if math.IsInf(c.X, 0) {
		fix = true
		c.X = 0
	}
	if math.IsInf(c.Y, 0) {
		fix = true
		c.Y = 0
	}
	return fix
}

func (c *Cord) round() {
	c.X = dround(c.X)
	c.Y = dround(c.Y)
}
