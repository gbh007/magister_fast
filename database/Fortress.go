package database

import ()

// Fortress структура для крепостей
type Fortress struct {
	Cord
	ID            int64   `json:"id"`
	Damage        float64 `json:"dmg"`
	AttackRadius  float64 `json:"radius"`
	LiveTime      int64   `json:"live_time"`
	DefeatedLevel float64 `json:"def_lv"`
	DefeatedCount int64   `json:"def_cnt"`
}

func (f *Fortress) round() {
	f.Cord.round()
	f.AttackRadius = dround(f.AttackRadius)
	f.Damage = dround(f.Damage)
	f.DefeatedLevel = dround(f.DefeatedLevel)
}
