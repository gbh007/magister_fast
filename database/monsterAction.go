package database

func monsterAction(db *Database) {
	// движение от ближайшего героя и крепости
	for _, monster := range db.Monsters {
		if monster.HP/monster.HPMax < db.World.MonsterHPRun {
			var id int
			id = -1
			var coef float64
			shifts := make([]cordInterface, 0)
			for ind, hero := range db.Heroes {
				if hero.HP <= 0 {
					continue
				}
				if r := radius(monster, hero); id == -1 || r < coef {
					id = ind
					coef = r
				}
			}
			if id != -1 {
				sh := getShiftCord(db.Heroes[id], monster, -1)
				shifts = append(shifts, inverseShiftCord(sh))
			}
			id = -1
			coef = 0
			for ind, fortress := range db.Fortresses {
				if r := radius(monster, fortress); id == -1 || r < coef {
					id = ind
					coef = r
				}
			}
			if id != -1 {
				sh := getShiftCord(db.Fortresses[id], monster, -1)
				shifts = append(shifts, inverseShiftCord(sh))
			}
			sh := truncateShist(monster.MaxMove, shifts...)
			monster.X += sh.GetX()
			monster.Y += sh.GetY()
			monster.fix()
			continue
		}
		// атака ближайшего героя
		var id int
		id = -1
		var coef float64
		for ind, hero := range db.Heroes {
			if radius(monster, hero) < monster.AttackRadius {
				if id == -1 || (hero.HP > 0 && hero.HP < coef) {
					id = ind
					coef = hero.HP
				}
			}
		}
		if id != -1 {
			hero := db.Heroes[id]
			dmg := monster.Damage * db.World.GetRand()
			hero.HP -= dmg
			db.ALog.Attack(db.World.Step, monster.ID, hero.ID, dmg)
			if hero.HP <= 0 {
				monster.HP += hero.HPMax * db.World.MonsterVamp * db.World.GetRand()
				monster.HPMax += hero.HPMax * db.World.MonsterVamp * db.World.GetRand()
				monster.HPPerStep += hero.HPPerStep * db.World.MonsterVamp * db.World.GetRand()
				monster.Damage += hero.Damage * db.World.MonsterVamp * db.World.GetRand()
				monster.DefeatedCount++
				monster.DefeatedLevel += weakMax(hero)
				db.ALog.Kill(db.World.Step, monster.ID, hero.ID)
				db.NeedClear()
			}
			continue
		}
		// движение с слабому герою
		id = -1
		coef = 1
		for ind, hero := range db.Heroes {
			if hero.HP <= 0 {
				continue
			}
			if w := weakF(hero, monster); w < coef || id == -1 {
				id = ind
				coef = w
			}
		}
		if id != -1 {
			sh := getShiftCord(db.Heroes[id], monster, monster.MaxMove)
			monster.X += sh.GetX()
			monster.Y += sh.GetY()
			monster.fix()
			continue
		}
	}
}
