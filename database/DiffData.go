package database

// SD структура для фиксации поля при шаге
type SD struct {
	Step int64       `json:"x"`
	Val  interface{} `json:"y"`
}

// DiffData структура для хранения изменений
type DiffData struct {
	Data map[int64]map[string][]SD
}

func newDD() *DiffData {
	return &DiffData{
		Data: make(map[int64]map[string][]SD),
	}
}
func (dd *DiffData) update(db *Database) {
	dd.updateEntity(db.World.Step, db.Monsters)
	dd.updateEntity(db.World.Step, db.Heroes)
	dd.updateFortress(db.World.Step, db.Fortresses)
}

func (dd *DiffData) updateFortress(step int64, fortresses []*Fortress) {
	for _, fort := range fortresses {
		if _, ok := dd.Data[fort.ID]; !ok {
			dd.Data[fort.ID] = make(map[string][]SD)
		}
		// обновление данных
		// live_time
		/*if _, ok := dd.Data[fort.ID]["live_time"]; !ok {
			dd.Data[fort.ID]["live_time"] = make([]SD, 0)
		}
		arr := dd.Data[fort.ID]["live_time"]
		if len(arr) == 0 || arr[len(arr)-1].Val != fort.LiveTime {
			dd.Data[fort.ID]["live_time"] = append(arr, SD{Step: step, Val: fort.LiveTime})
		}*/
		// radius
		if _, ok := dd.Data[fort.ID]["radius"]; !ok {
			dd.Data[fort.ID]["radius"] = make([]SD, 0)
		}
		arr := dd.Data[fort.ID]["radius"]
		if len(arr) == 0 || arr[len(arr)-1].Val != fort.AttackRadius {
			dd.Data[fort.ID]["radius"] = append(arr, SD{Step: step, Val: fort.AttackRadius})
		}
		// dmg
		if _, ok := dd.Data[fort.ID]["dmg"]; !ok {
			dd.Data[fort.ID]["dmg"] = make([]SD, 0)
		}
		arr = dd.Data[fort.ID]["dmg"]
		if len(arr) == 0 || arr[len(arr)-1].Val != fort.Damage {
			dd.Data[fort.ID]["dmg"] = append(arr, SD{Step: step, Val: fort.Damage})
		}
		// def_lv
		if _, ok := dd.Data[fort.ID]["def_lv"]; !ok {
			dd.Data[fort.ID]["def_lv"] = make([]SD, 0)
		}
		arr = dd.Data[fort.ID]["def_lv"]
		if len(arr) == 0 || arr[len(arr)-1].Val != fort.DefeatedLevel {
			dd.Data[fort.ID]["def_lv"] = append(arr, SD{Step: step, Val: fort.DefeatedLevel})
		}
		// def_cnt
		if _, ok := dd.Data[fort.ID]["def_cnt"]; !ok {
			dd.Data[fort.ID]["def_cnt"] = make([]SD, 0)
		}
		arr = dd.Data[fort.ID]["def_cnt"]
		if len(arr) == 0 || arr[len(arr)-1].Val != fort.DefeatedCount {
			dd.Data[fort.ID]["def_cnt"] = append(arr, SD{Step: step, Val: fort.DefeatedCount})
		}
	}
}
func (dd *DiffData) updateEntity(step int64, entitys []*Entity) {
	for _, ent := range entitys {
		if _, ok := dd.Data[ent.ID]; !ok {
			dd.Data[ent.ID] = make(map[string][]SD)
		}
		// обновление данных
		// hp
		if _, ok := dd.Data[ent.ID]["hp"]; !ok {
			dd.Data[ent.ID]["hp"] = make([]SD, 0)
		}
		arr := dd.Data[ent.ID]["hp"]
		if len(arr) == 0 || arr[len(arr)-1].Val != ent.HP {
			dd.Data[ent.ID]["hp"] = append(arr, SD{Step: step, Val: ent.HP})
		}
		// hp_max
		if _, ok := dd.Data[ent.ID]["hp_max"]; !ok {
			dd.Data[ent.ID]["hp_max"] = make([]SD, 0)
		}
		arr = dd.Data[ent.ID]["hp_max"]
		if len(arr) == 0 || arr[len(arr)-1].Val != ent.HPMax {
			dd.Data[ent.ID]["hp_max"] = append(arr, SD{Step: step, Val: ent.HPMax})
		}
		// hp_per_step
		if _, ok := dd.Data[ent.ID]["hp_per_step"]; !ok {
			dd.Data[ent.ID]["hp_per_step"] = make([]SD, 0)
		}
		arr = dd.Data[ent.ID]["hp_per_step"]
		if len(arr) == 0 || arr[len(arr)-1].Val != ent.HPPerStep {
			dd.Data[ent.ID]["hp_per_step"] = append(arr, SD{Step: step, Val: ent.HPPerStep})
		}
		// max_move
		if _, ok := dd.Data[ent.ID]["max_move"]; !ok {
			dd.Data[ent.ID]["max_move"] = make([]SD, 0)
		}
		arr = dd.Data[ent.ID]["max_move"]
		if len(arr) == 0 || arr[len(arr)-1].Val != ent.MaxMove {
			dd.Data[ent.ID]["max_move"] = append(arr, SD{Step: step, Val: ent.MaxMove})
		}
		// radius
		if _, ok := dd.Data[ent.ID]["radius"]; !ok {
			dd.Data[ent.ID]["radius"] = make([]SD, 0)
		}
		arr = dd.Data[ent.ID]["radius"]
		if len(arr) == 0 || arr[len(arr)-1].Val != ent.AttackRadius {
			dd.Data[ent.ID]["radius"] = append(arr, SD{Step: step, Val: ent.AttackRadius})
		}
		// dmg
		if _, ok := dd.Data[ent.ID]["dmg"]; !ok {
			dd.Data[ent.ID]["dmg"] = make([]SD, 0)
		}
		arr = dd.Data[ent.ID]["dmg"]
		if len(arr) == 0 || arr[len(arr)-1].Val != ent.Damage {
			dd.Data[ent.ID]["dmg"] = append(arr, SD{Step: step, Val: ent.Damage})
		}
		// def_lv
		if _, ok := dd.Data[ent.ID]["def_lv"]; !ok {
			dd.Data[ent.ID]["def_lv"] = make([]SD, 0)
		}
		arr = dd.Data[ent.ID]["def_lv"]
		if len(arr) == 0 || arr[len(arr)-1].Val != ent.DefeatedLevel {
			dd.Data[ent.ID]["def_lv"] = append(arr, SD{Step: step, Val: ent.DefeatedLevel})
		}
		// def_cnt
		if _, ok := dd.Data[ent.ID]["def_cnt"]; !ok {
			dd.Data[ent.ID]["def_cnt"] = make([]SD, 0)
		}
		arr = dd.Data[ent.ID]["def_cnt"]
		if len(arr) == 0 || arr[len(arr)-1].Val != ent.DefeatedCount {
			dd.Data[ent.ID]["def_cnt"] = append(arr, SD{Step: step, Val: ent.DefeatedCount})
		}
	}
}
