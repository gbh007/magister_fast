package database

import (
	"math"
)

func dround(x float64) float64 { return math.Round(x*1000) / 1000 }
func dsafe(x float64) float64 {
	if math.IsInf(x, 0) {
		x = 99999
	}
	if math.IsNaN(x) {
		x = 0
	}
	return x
}

type cordInterface interface {
	GetX() float64
	GetY() float64
}

func radius(c1, c2 cordInterface) float64 {
	if c2 == nil { // радиус до центра координат; или длина гипотенузы
		return math.Sqrt(
			c1.GetX()*c1.GetX() +
				c1.GetY()*c1.GetY(),
		)
	}
	return math.Sqrt(
		(c1.GetX()-c2.GetX())*(c1.GetX()-c2.GetX()) +
			(c1.GetY()-c2.GetY())*(c1.GetY()-c2.GetY()),
	)
}

func getShiftCord(target, current cordInterface, dd float64) cordInterface {
	r := radius(target, current)
	dx := target.GetX() - current.GetX()
	dy := target.GetY() - current.GetY()
	a := dd / r
	if a > 1 || dd < 0 {
		a = 1
	}
	return &Cord{
		X: dx * a,
		Y: dy * a,
	}
}
func truncateShist(dd float64, cords ...cordInterface) cordInterface {
	c := &Cord{}
	for _, cord := range cords {
		c.X += cord.GetX()
		c.Y += cord.GetY()
	}
	r := radius(c, nil)
	a := dd / r
	if a > 1 {
		a = 1
	}
	return &Cord{
		X: c.GetX() * a,
		Y: c.GetY() * a,
	}
}
func inverseShiftCord(c cordInterface) cordInterface {
	return &Cord{
		X: -c.GetX(),
		Y: -c.GetY(),
	}
}

func weakMax(target *Entity) float64 {
	res := 0.0
	if tmp := math.Log10(target.HPMax); !math.IsNaN(tmp) {
		res += tmp
	}
	if tmp := math.Log10(target.Damage); !math.IsNaN(tmp) {
		res += tmp
	}
	return res
}

func weak(target, current *Entity) float64 {
	return (target.HP * target.Damage) / (current.HP * current.Damage)
}

func weakF(target, current *Entity) float64 {
	res := weak(target, current) * radius(target, current)
	if res < 0.0001 {
		res = 0.0001
	}
	if math.IsNaN(res) || math.IsInf(res, 0) {
		res = 100000
	}
	return res
}
