package database

func heroAction(db *Database) {
	for _, hero := range db.Heroes {
		// движение к ближайшей крепости если мало хп
		if hero.HP/hero.HPMax < db.World.HeroHPRun {
			var id = -1
			var coef float64
			for ind, fortress := range db.Fortresses {
				if r := radius(hero, fortress); id == -1 || r < coef {
					id = ind
					coef = r
				}
			}
			if id != -1 {
				sh := getShiftCord(db.Fortresses[id], hero, hero.MaxMove)
				hero.X += sh.GetX()
				hero.Y += sh.GetY()
			} else {
				sh := getShiftCord(&Cord{X: 0, Y: 0}, hero, hero.MaxMove)
				hero.X += sh.GetX()
				hero.Y += sh.GetY()
			}
			hero.fix()
			continue
		}
		// атака ближайщего монстра
		var id = -1
		var coef float64
		for ind, monster := range db.Monsters {
			if radius(hero, monster) < hero.AttackRadius {
				if id == -1 || (monster.HP > 0 && monster.HP < coef) {
					id = ind
					coef = monster.HP
				}
			}
		}
		if id != -1 {
			monster := db.Monsters[id]
			dmg := hero.Damage * db.World.GetRand()
			monster.HP -= dmg
			db.ALog.Attack(db.World.Step, hero.ID, monster.ID, dmg)
			if monster.HP <= 0 {
				hero.HP += monster.HPMax * db.World.HeroVamp * db.World.GetRand()
				hero.HPMax += monster.HPMax * db.World.HeroVamp * db.World.GetRand()
				hero.HPPerStep += monster.HPPerStep * db.World.HeroVamp * db.World.GetRand()
				hero.Damage += monster.Damage * db.World.HeroVamp * db.World.GetRand()
				hero.DefeatedCount++
				hero.DefeatedLevel += weakMax(monster)
				db.ALog.Kill(db.World.Step, hero.ID, monster.ID)
				db.NeedClear()
			}
			continue
		}
		// движение к слабому монстру
		id = -1
		coef = 1
		for ind, monster := range db.Monsters {
			if monster.HP < 0 {
				continue
			}
			if w := weakF(monster, hero); w < coef || id == -1 {
				id = ind
				coef = w
			}
		}
		if id != -1 {
			sh := getShiftCord(db.Monsters[id], hero, hero.MaxMove)
			hero.X += sh.GetX()
			hero.Y += sh.GetY()
			hero.fix()
			continue
		}
	}
}
