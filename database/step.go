package database

import (
	"math"
)

// Step соверщает новую итерацию модлирования
func (db *Database) Step() {
	db.World.Step++
	db.DeadClear()
	heroAction(db)
	monsterAction(db)
	fortressAction(db)
	for _, mg := range db.MG {
		if mg.Start == db.World.Step {
			res := mg.Gen(db.World.LastID)
			if len(res) > 0 {
				db.World.LastID += int64(len(res))
				db.AddMonster(res...)
			}
			mg.Start += mg.Period
		}
		if mg.Start < db.World.Step {
			db.NeedClear()
		}
	}
	for _, hg := range db.HG {
		if hg.Start == db.World.Step {
			res := hg.Gen(db.World.LastID)
			if len(res) > 0 {
				db.World.LastID += int64(len(res))
				db.AddHero(res...)
			}
			hg.Start += hg.Period
		}
		if hg.Start < db.World.Step {
			db.NeedClear()
		}
	}
	for _, fg := range db.FG {
		if fg.Start == db.World.Step {
			res := fg.Gen(db.World.LastID)
			if len(res) > 0 {
				db.World.LastID += int64(len(res))
				db.AddFortress(res...)
			}
			fg.Start += fg.Period
		}
		if fg.Start < db.World.Step {
			db.NeedClear()
		}
	}
	for _, monster := range db.Monsters {
		if monster.HP <= 0 {
			db.NeedClear()
			continue
		}
		monster.HP = math.Min(monster.HP+monster.HPPerStep, monster.HPMax)
		monster.round()
	}
	for _, hero := range db.Heroes {
		if hero.HP <= 0 {
			db.NeedClear()
			continue
		}
		hero.HP = math.Min(hero.HP+hero.HPPerStep, hero.HPMax)
		hero.round()
	}
	for _, fortress := range db.Fortresses {
		fortress.LiveTime--
		if fortress.LiveTime < 1 {
			db.ALog.Dead(db.World.Step, fortress.ID)
			db.NeedClear()
			continue
		}
		fortress.round()
	}
}
