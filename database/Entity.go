package database

import ()

// Entity структура для существ (монстры/герои)
type Entity struct {
	Cord
	ID            int64   `json:"id"`
	HP            float64 `json:"hp"`
	HPMax         float64 `json:"hp_max"`
	HPPerStep     float64 `json:"hp_per_step"`
	MaxMove       float64 `json:"max_move"`
	AttackRadius  float64 `json:"radius"`
	Damage        float64 `json:"dmg"`
	DefeatedLevel float64 `json:"def_lv"`
	DefeatedCount int64   `json:"def_cnt"`
}

func (e *Entity) round() {
	e.Cord.round()
	e.HP = dround(e.HP)
	e.HPMax = dround(e.HPMax)
	e.HPPerStep = dround(e.HPPerStep)
	e.MaxMove = dround(e.MaxMove)
	e.AttackRadius = dround(e.AttackRadius)
	e.Damage = dround(e.Damage)
	e.DefeatedLevel = dround(e.DefeatedLevel)
}
