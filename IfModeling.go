package main

import (
	"fmt"
	"magister_fast/database"
	"math"
	"math/rand"
	"time"
)

type IfModeling struct {
	db *database.Database
}

func (im *IfModeling) Make() error {
	return nil
}

func (im *IfModeling) Before(ename string, remod bool) error {
	rand.Seed(time.Now().Unix())
	im.db = database.NewDatabase(baseDir)
	if remod {
	} else {
		// добавление генератора монстров
		im.db.AddMG(&database.EntityGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: 50,
				Start:  1,
				Count:  20,
				XDev:   1000,
				XMean:  0,
				YDev:   1000,
				YMean:  0,
			},
			DamageMean:       40,
			DamageDev:        5,
			MaxHPMean:        100,
			MaxHPDev:         20,
			HPPSMean:         10,
			HPPSDev:          3,
			AttackRadiusMean: 10,
			AttackRadiusDev:  3,
			MaxMoveMean:      5,
			MaxMoveDev:       1,
		})
		// добавление генератора героев
		im.db.AddHG(&database.EntityGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: 50,
				Start:  1,
				Count:  20,
				XDev:   1000,
				XMean:  0,
				YDev:   1000,
				YMean:  0,
			},
			DamageMean:       50,
			DamageDev:        5,
			MaxHPMean:        60,
			MaxHPDev:         20,
			HPPSMean:         10,
			HPPSDev:          3,
			AttackRadiusMean: 10,
			AttackRadiusDev:  3,
			MaxMoveMean:      10,
			MaxMoveDev:       2,
		})
		// добавление генератора крепостей
		im.db.AddFG(&database.FortressGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: 100,
				Start:  1,
				Count:  5,
				XDev:   100,
				XMean:  0,
				YDev:   100,
				YMean:  0,
			},
			DamageMean:       50,
			DamageDev:        10,
			AttackRadiusMean: 20,
			AttackRadiusDev:  5,
			LiveTimeStart:    500,
			LiveTimeLen:      500,
		})
		im.db.World = database.World{
			HeroHPRun:    0.3,
			HeroVamp:     vampire,
			MonsterHPRun: 0.15,
			MonsterVamp:  vampire,
			LastID:       0,
			Step:         0,
			RandomStart:  0.75,
			RandomLen:    0.5,
		}
	}
	return nil
}
func (im *IfModeling) Step(i int) error {
	if i%100 != 0 {
		return nil
	}
	im.db.Update(false)
	cf := float64(im.db.World.HeroCount) / float64(im.db.World.MonsterCount)
	if math.IsInf(cf, 0) || math.IsNaN(cf) {
		im.db.MG[0].Count = 20
		cf = 1
	}
	im.db.MG[0].Count = int(float64(im.db.MG[0].Count) * (0.8*cf + 0.2))
	return nil
}
func (im *IfModeling) Info() string {
	return fmt.Sprintf(
		"step %d monster count %d hero count %d fortress count %d",
		im.db.World.Step,
		len(im.db.Monsters),
		len(im.db.Heroes),
		len(im.db.Fortresses),
	)
}
func (im *IfModeling) DB() *database.Database { return im.db }
