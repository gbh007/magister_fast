package main

import (
	"encoding/json"
	"io"
	"log"
	"os"
)

func saveToW(w io.Writer, data interface{}) error {
	err := json.NewEncoder(w).Encode(&data)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func load(filename string, data interface{}) error {
	if file, err := os.Open(filename); err == nil {
		err := json.NewDecoder(file).Decode(&data)
		file.Close()
		if err != nil {
			log.Println(err)
			return err
		}
	} else {
		log.Println(err)
		return err
	}
	return nil
}
