package main

import (
	"fmt"
	"magister_fast/database"
	"math"
	"math/rand"
	"sort"
	"time"
)

type individ struct {
	heroC     int
	monsterC  int
	fortressC int
	born      int64
	quality   float64
}

func (i *individ) new(i2 *individ) *individ {
	return &individ{
		//heroC:     (i.heroC + i2.heroC) / 2,
		monsterC:  (i.monsterC + i2.monsterC) / 2,
		fortressC: (i.fortressC + i2.fortressC) / 2,
	}
}

func (i *individ) mut(mut float64) *individ {
	/*if mut < rand.Float64() {
		i.heroC = int(float64(i.heroC) * (rand.Float64()*0.5 + 0.75))
		return i
	}*/
	if mut < rand.Float64() {
		i.monsterC = int(float64(i.monsterC) * (rand.Float64()*0.5 + 0.75))
		return i
	}
	if mut < rand.Float64() {
		i.fortressC = int(float64(i.fortressC) * (rand.Float64()*0.5 + 0.75))
		return i
	}
	return i
}

type GenModeling struct {
	mut        float64
	swapTime   int64
	population []*individ
	current    *individ
	db         *database.Database
}

func (ga *GenModeling) Before(ename string, remod bool) error {
	rand.Seed(time.Now().Unix())
	ga.db = database.NewDatabase(baseDir)
	if remod {
	} else {
		ga.db.AddMG(&database.EntityGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: 50,
				Start:  1,
				Count:  20,
				XDev:   1000,
				XMean:  0,
				YDev:   1000,
				YMean:  0,
			},
			DamageMean:       40,
			DamageDev:        5,
			MaxHPMean:        100,
			MaxHPDev:         20,
			HPPSMean:         10,
			HPPSDev:          3,
			AttackRadiusMean: 10,
			AttackRadiusDev:  3,
			MaxMoveMean:      5,
			MaxMoveDev:       1,
		})
		// добавление генератора героев
		ga.db.AddHG(&database.EntityGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: 50,
				Start:  1,
				Count:  20,
				XDev:   1000,
				XMean:  0,
				YDev:   1000,
				YMean:  0,
			},
			DamageMean:       50,
			DamageDev:        5,
			MaxHPMean:        60,
			MaxHPDev:         20,
			HPPSMean:         10,
			HPPSDev:          3,
			AttackRadiusMean: 10,
			AttackRadiusDev:  3,
			MaxMoveMean:      10,
			MaxMoveDev:       2,
		})
		// добавление генератора крепостей
		ga.db.AddFG(&database.FortressGenerator{
			SimpleGenerator: database.SimpleGenerator{
				Period: 100,
				Start:  1,
				Count:  5,
				XDev:   100,
				XMean:  0,
				YDev:   100,
				YMean:  0,
			},
			DamageMean:       50,
			DamageDev:        10,
			AttackRadiusMean: 20,
			AttackRadiusDev:  5,
			LiveTimeStart:    500,
			LiveTimeLen:      500,
		})
		ga.db.World = database.World{
			HeroHPRun:    0.3,
			HeroVamp:     vampire,
			MonsterHPRun: 0.15,
			MonsterVamp:  vampire,
			LastID:       0,
			Step:         0,
			RandomStart:  0.75,
			RandomLen:    0.5,
		}
	}
	ga.current = &individ{
		heroC:     20,
		monsterC:  20,
		fortressC: 5,
	}
	return nil
}
func (ga *GenModeling) clearPop(db *database.Database) {
	np := make([]*individ, 0)
	for _, i := range ga.population {
		if i.born+1000 > db.World.Step {
			np = append(np, i)
		}
	}
	ga.population = np
}

func (ga *GenModeling) newCH(db *database.Database) {
	ga.clearPop(db)
	ga.current.quality = float64(db.World.HeroCount+db.World.MonsterCount) / math.Max(
		float64(2*db.World.HeroCount),
		float64(2*db.World.MonsterCount),
	)
	ga.population = append(ga.population, ga.current)
	if len(ga.population) > 1 {
		sort.Slice(ga.population, func(i, j int) bool {
			return ga.population[i].quality > ga.population[j].quality
		})
		ga.current = ga.population[0].new(ga.population[1])
	} else {
		ga.current = ga.population[0].new(ga.population[0])
	}
	ga.current.mut(ga.mut)
	ga.current.born = db.World.Step
	//db.HG[0].Count = ga.current.heroC
	db.MG[0].Count = ga.current.monsterC
	db.FG[0].Count = ga.current.fortressC
}
func (ga *GenModeling) Make() error { return nil }

func (ga *GenModeling) Step(i int) error {
	if (i+1)%50 == 0 {
		ga.newCH(ga.db)
	}
	return nil
}

func (ga *GenModeling) Info() string {
	return fmt.Sprintf(
		"step %d monster count %d hero count %d fortress count %d",
		ga.db.World.Step,
		len(ga.db.Monsters),
		len(ga.db.Heroes),
		len(ga.db.Fortresses),
	)
}
func (ga *GenModeling) DB() *database.Database { return ga.db }
