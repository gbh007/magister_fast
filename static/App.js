function getRandomColor() {
  let letters = "0123456789ABCDEF".split("");
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

var app = new Vue({
  el: "#app",
  data: {
    statList: [
      "def_cnt",
      "def_lv",
      "dmg",
      "hp",
      "hp_max",
      "hp_per_step",
      "id",
      "max_move",
      "radius",
      "x",
      "y",
      "live_time"
    ],
    req: {
      start: 0,
      end: 1000,
      exp: "exp"
    },
    view: {
      monster: true,
      hero: true,
      colony: true
    },
    sh: {
      hp: {
        hp: true,
        hp_max: true,
        hp_per_step: true,
        live_time: true
      },
      dmg: {
        dmg: true,
        rad: true,
        def: true
      },
      map: {
        hero: true,
        monster: true,
        fortress: true
      },
      avg: {}
    },
    step: 0,
    ddstep: 1000,
    chart: null,
    chart1: null,
    chart2: null,
    chart3: null,
    chart4: null,
    chart5: null,
    chartType: {
      x: "linear",
      y: "linear"
    },
    ent: {
      id: 0
    },
    hist: {
      data: {},
      count: 120,
      start: 0,
      end: 12000
    },
    hfull: {},
    corMap: {
      st1: "def_cnt",
      st2: "id"
    },
    entitys: [],
    avgStatData: {},
    expList: []
  },
  mounted: function() {
    this.load();
    this.loadAvgStat();
    axios.post("/exp/get").then(response => {
      this.expList = response.data;
    });
    axios.post("/hist/get/many", { count: this.hist.count }).then(response => {
      this.hfull = response.data;
    });
  },
  computed: {
    eInfo(self) {
      let res = self.entitys.filter(e => e.id == self.ent.id);
      if (res.length == 1) {
        return res[0];
      } else {
        return { id: 0 };
      }
    }
  },
  methods: {
    load: function() {
      this.entitys = [];
      let self = this;
      axios.post("/info/get", { exp: self.req.exp }).then(function(response) {
        response.data.forEach(e => self.entitys.push(e));
      });
    },
    getStat: function(id) {
      return new Promise((resolve, reject) => {
        let self = this;
        axios
          .post("/stat/get", {
            id: id,
            exp: self.req.exp
          })
          .then(function(response) {
            resolve(response.data);
          })
          .catch(err => {
            reject(err);
          });
      });
    },
    loadAvgStat: function() {
      return new Promise((resolve, reject) => {
        let self = this;
        axios
          .post("/avg/get", {
            exp: self.req.exp
          })
          .then(function(response) {
            Vue.set(self.sh, "avg", {});
            self.avgStatData = response.data;
            Object.keys(response.data).forEach(e =>
              Vue.set(self.sh.avg, e, false)
            );
            resolve(response.data);
          })
          .catch(err => {
            reject(err);
          });
      });
    },
    showHP: function() {
      let datasets = [];
      this.getStat(this.ent.id).then(data => {
        if (this.sh.hp.hp) {
          datasets.push({
            label: "HP",
            borderColor: "green",
            showLine: true,
            data: data["hp"]
          });
        }
        if (this.sh.hp.hp_max) {
          datasets.push({
            label: "MAX HP",
            borderColor: "red",
            showLine: true,
            data: data["hp_max"]
          });
        }
        if (this.sh.hp.hp_per_step) {
          datasets.push({
            label: "HP per step",
            borderColor: "blue",
            showLine: true,
            data: data["hp_per_step"]
          });
        }
        if (this.sh.hp.live_time) {
          datasets.push({
            label: "Liveteme (for fortress)",
            borderColor: "orange",
            showLine: true,
            data: data["live_time"]
          });
        }
        this.showLineDatasets(datasets, "шаг", "значение");
      });
    },
    showAvg: function() {
      let datasets = [];
      let data = this.avgStatData;
      Object.keys(this.sh.avg).forEach(stat => {
        if (this.sh.avg[stat]) {
          datasets.push({
            label: stat,
            borderColor: getRandomColor(),
            showLine: true,
            fill: false,
            data: data[stat]
          });
        }
      });
      if (this.chart3 == null) {
        this.chart3 = Chart.Scatter(this.$refs.gr3, {});
      }
      this.show(this.chart3, datasets, "шаг", "значение", null, null);
    },
    showDMG: function() {
      let datasets = [];
      this.getStat(this.ent.id).then(data => {
        if (this.sh.dmg.dmg) {
          datasets.push({
            label: "Damage",
            borderColor: "red",
            showLine: true,
            data: data["dmg"]
          });
        }
        if (this.sh.dmg.rad) {
          datasets.push({
            label: "Radius",
            borderColor: "green",
            showLine: true,
            data: data["radius"]
          });
        }
        if (this.sh.dmg.def) {
          datasets.push({
            label: "Defeated count",
            borderColor: "blue",
            showLine: true,
            data: data["def_cnt"]
          });
        }
        this.showLineDatasets(datasets, "step", "points");
      });
    },
    showCF: function(data) {
      if (this.chart2 == null) {
        this.chart2 = Chart.Bubble(this.$refs.gr2, {});
      }
      let datasets = [];
      let self = this;
      let getData = function(dt) {
        let dd = [];
        dt.forEach(e =>
          self.corMap.st1 in e && self.corMap.st2 in e
            ? dd.push({ x: e[self.corMap.st1], y: e[self.corMap.st2] })
            : ""
        );
        return dd;
      };
      if (this.sh.map.hero) {
        datasets.push({
          label: "hero",
          borderColor: "green",
          data: getData(data.heroes)
        });
      }
      if (this.sh.map.monster) {
        datasets.push({
          label: "monster",
          borderColor: "red",
          data: getData(data.monsters)
        });
      }
      if (this.sh.map.fortress) {
        datasets.push({
          label: "fortress",
          borderColor: "blue",
          data: getData(data.fortresses)
        });
      }
      this.show(
        this.chart2,
        datasets,
        this.corMap.st1,
        this.corMap.st2,
        null,
        null
      );
    },
    showStep: function(data) {
      if (this.chart == null) {
        this.chart = Chart.Bubble(this.$refs.gr, {});
      }
      let datasets = [];
      let getData = function(dt) {
        let dd = [];
        dt.forEach(e => dd.push({ x: e.x, y: e.y }));
        return dd;
      };
      if (this.sh.map.hero) {
        datasets.push({
          label: "hero",
          borderColor: "green",
          data: getData(data.heroes)
        });
      }
      if (this.sh.map.monster) {
        datasets.push({
          label: "monster",
          borderColor: "red",
          data: getData(data.monsters)
        });
      }
      if (this.sh.map.fortress) {
        datasets.push({
          label: "fortress",
          borderColor: "blue",
          data: getData(data.fortresses)
        });
      }
      this.show(this.chart, datasets, "X", "Y");
    },
    showLineDatasets: function(datasets, xname, yname) {
      if (this.chart1 == null) {
        this.chart1 = Chart.Scatter(this.$refs.gr1, {});
      }
      this.show(this.chart1, datasets, xname, yname, null, null);
    },
    mapShow: function() {
      let data = {};
      let self = this;
      axios
        .post("/snapshot/get", { exp: self.req.exp, step: self.step })
        .then(function(response) {
          data = response.data;
          self.showStep(data);
          self.showCF(data);
        });
    },
    showClear: function() {
      if (this.chart4 == null) {
        this.chart4 = Chart.Line(this.$refs.gr4, {});
      }
      axios.post("/hist/get", { count: this.hist.count }).then(response => {
        let data = [];
        for (let i = 0; i < response.data.count + 2; i++) {
          data.push({
            x: response.data.len * i + response.data.len / 2,
            y: response.data.hist[i]
          });
        }
        this.show(
          this.chart4,
          [
            {
              label: "плотность распределения",
              borderColor: "blue",
              showLine: true,
              fill: false,
              data: data
            }
          ],
          "шаг",
          "количество",
          null,
          null
        );
      });
    },
    showFullClear: function() {
      if (this.chart5 == null) {
        this.chart5 = Chart.Line(this.$refs.gr5, {});
      }
      let res = [];
      this.hfull.forEach(e => {
        let data = [];
        let sum = 0;
        for (let i = 0; i < e.count + 2; i++) {
          if (
            this.hist.start < e.len * i + e.len / 2 &&
            e.len * i + e.len / 2 < this.hist.end
          ) {
            data.push({
              x: e.len * i + e.len / 2,
              y: e.hist[i]
            });
            sum += e.hist[i];
          }
        }
        data.forEach(e => (e.y = e.y / (sum > 0 ? sum : 1)));
        res.push({
          label: e.name,
          borderColor: getRandomColor(),
          showLine: true,
          fill: false,
          data: data
        });
      });
      this.show(this.chart5, res, "шаг", "плотность", null, null);
    },
    show: function(
      chart,
      datasets,
      xname,
      yname,
      xticks = { min: -5000, max: 5000 },
      yticks = { min: -5000, max: 5000 }
    ) {
      chart.options = {
        animation: {
          duration: 0
        },
        title: {
          display: false
        },
        scales: {
          xAxes: [
            {
              type: this.chartType.x,
              scaleLabel: {
                display: true,
                labelString: xname
              }
            }
          ],
          yAxes: [
            {
              type: this.chartType.y,
              scaleLabel: {
                display: true,
                labelString: yname
              }
            }
          ]
        }
      };
      if (xticks != null) {
        chart.options.scales.xAxes[0].ticks = xticks;
      }
      if (yticks != null) {
        chart.options.scales.yAxes[0].ticks = yticks;
      }
      chart.data = {
        datasets: datasets
      };
      chart.update();
    }
  }
});
